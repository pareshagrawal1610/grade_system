from django.urls import path
from . import views
from .views import StudentViewSet,SubjectViewSet,FacultyViewSet,GradesViewSet

urlpatterns = [

    path('students/',StudentViewSet.as_view({'get':'list','post':'create'})),
    path('students/<int:pk>/',StudentViewSet.as_view({'get':'retrieve','put':'update','delete':'destroy'})),

    path('subjects/',SubjectViewSet.as_view({'get':'list','post':'create'})),
    path('subjects/<int:pk>/',SubjectViewSet.as_view({'get':'retrieve'})),
    
    path('facultys/',FacultyViewSet.as_view({'get':'list','post':'create'})),
    path('facultys/<int:pk>/',FacultyViewSet.as_view({'get':'retrieve','put':'update','delete':'destroy'})),
    
    path('grades/',GradesViewSet.as_view({'get':'list','post':'create'})),
    path('grades/<int:pk>/',GradesViewSet.as_view({'get':'retrieve','put':'update'})),

]

