from django.contrib import admin
from .models import Student,Faculty,Subject,Grades

class StudentAdmin(admin.ModelAdmin):
    list_display = ('stdId','stdName')

class FacultyAdmin(admin.ModelAdmin):
    list_display = ('fctId','fctName','subject')

class SubjectAdmin(admin.ModelAdmin):
    list_display = ('subId','subName')

class GradesAdmin(admin.ModelAdmin):
    list_display = ('student','subject','grade')

admin.site.register(Student,StudentAdmin)
admin.site.register(Faculty,FacultyAdmin)
admin.site.register(Subject,SubjectAdmin)
admin.site.register(Grades,GradesAdmin)