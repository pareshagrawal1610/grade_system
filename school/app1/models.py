from django.db import models

class Student(models.Model):
    stdId = models.IntegerField()
    stdName = models.CharField(max_length = 255)

    def __str__(self):
        return self.stdName

class Subject(models.Model):
    subId = models.IntegerField()
    subName = models.CharField(max_length = 255)

    def __str__(self):
        return self.subName

class Faculty(models.Model):
    fctId = models.IntegerField()
    fctName = models.CharField(max_length = 255)
    subject = models.ForeignKey(Subject,on_delete=models.CASCADE)

    def __str__(self):
        return self.fctName

class Grades(models.Model):
    student = models.ForeignKey(Student,on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject,on_delete=models.CASCADE)
    grade = models.CharField(max_length = 255)

    def __str__(self):
        return self.grade 